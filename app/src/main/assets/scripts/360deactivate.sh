#!/system/bin/sh
#_____________________________________________
#--------------------------------------------------------------------------
#Copyright Team Nameless
#A tweak for Nameless Note 4 
#                     -  by Senthil360 -
#Credits - Nameless Team and also
#Paget96
#F4uzan
#Greekdragon
#_____________________________________________
#----------------------------------------------------------------------

busybox mount -o remount,rw /
busybox mount -o remount,rw rootfs
busybox mount -o remount,rw /system

rm -f /system/etc/cron.d/*
rm -rf /system/etc/cron.d
rm -f /data/cron-360.log
rm -f /data/Senthil360
rm -f /data/cron-sp8.log
rm -f /data/cron-360

reboot

