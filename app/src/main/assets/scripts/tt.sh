#!/system/bin/sh
export PATH=/system/bin:$PATH

mount -o rw,remount /system

cp -p /system/nameless/files/tt/floating_feature.xml /system/etc/floating_feature.xml

pkill zygote
