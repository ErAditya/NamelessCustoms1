#!/system/bin/sh

mount -o rw,remount /data

cp -rf /sdcard/namelessbackups/shared_prefs /data/data/com.wubydax.romcontrol

pkill zygote
