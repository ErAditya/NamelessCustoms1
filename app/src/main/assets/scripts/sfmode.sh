#!/system/bin/sh

PATH=$PATH:/system/xbin:/sbin:/vendor/bin:/system/sbin:/system/bin
if [ -d "/data/local/busybox*/xbin" ]; then 
	PATH=$PATH:`ls -d /data/local/busybox*/xbin 2>/dev/null`; 
fi

LOG=/data/local/Safemode_Reboot.log;

if [ -e $LOG ]; then
	rm $LOG;
fi;

setprop persist.sys.safemode 1

echo "" >> $LOG;
echo "$( date +"%m-%d-%Y %H:%M:%S" ) Safemode Reboot completed..." >> $LOG;

su -c reboot now
