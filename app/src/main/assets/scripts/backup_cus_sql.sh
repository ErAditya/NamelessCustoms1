#!/system/bin/sh

mount -o rw,remount /data

mkdir /sdcard/namelessbackups

cp -rf /data/data/com.wubydax.romcontrol/shared_prefs /sdcard/namelessbackups
